import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchoolViewSearchResultsComponent } from './school-view-search-results.component';

describe('SchoolViewSearchResultsComponent', () => {
  let component: SchoolViewSearchResultsComponent;
  let fixture: ComponentFixture<SchoolViewSearchResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchoolViewSearchResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchoolViewSearchResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
