import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RankPredictorThirdComponent } from './rank-predictor-third.component';

describe('RankPredictorThirdComponent', () => {
  let component: RankPredictorThirdComponent;
  let fixture: ComponentFixture<RankPredictorThirdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RankPredictorThirdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RankPredictorThirdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
