import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchoolComparisionComponent } from './school-comparision.component';

describe('SchoolComparisionComponent', () => {
  let component: SchoolComparisionComponent;
  let fixture: ComponentFixture<SchoolComparisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchoolComparisionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchoolComparisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
