import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchoolApplicationFormComponent } from './school-application-form.component';

describe('SchoolApplicationFormComponent', () => {
  let component: SchoolApplicationFormComponent;
  let fixture: ComponentFixture<SchoolApplicationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchoolApplicationFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchoolApplicationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
