import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudyAbroadDetailComponent } from './study-abroad-detail.component';

describe('StudyAbroadDetailComponent', () => {
  let component: StudyAbroadDetailComponent;
  let fixture: ComponentFixture<StudyAbroadDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudyAbroadDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudyAbroadDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
