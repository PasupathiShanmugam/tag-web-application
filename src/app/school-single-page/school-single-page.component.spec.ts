import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchoolSinglePageComponent } from './school-single-page.component';

describe('SchoolSinglePageComponent', () => {
  let component: SchoolSinglePageComponent;
  let fixture: ComponentFixture<SchoolSinglePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchoolSinglePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchoolSinglePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
