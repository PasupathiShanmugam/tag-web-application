import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsArticalComponent } from './news-artical.component';

describe('NewsArticalComponent', () => {
  let component: NewsArticalComponent;
  let fixture: ComponentFixture<NewsArticalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsArticalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsArticalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
