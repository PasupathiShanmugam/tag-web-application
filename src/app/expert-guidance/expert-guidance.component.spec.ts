import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpertGuidanceComponent } from './expert-guidance.component';

describe('ExpertGuidanceComponent', () => {
  let component: ExpertGuidanceComponent;
  let fixture: ComponentFixture<ExpertGuidanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpertGuidanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpertGuidanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
