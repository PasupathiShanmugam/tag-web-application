import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RankPredictorSecondComponent } from './rank-predictor-second.component';

describe('RankPredictorSecondComponent', () => {
  let component: RankPredictorSecondComponent;
  let fixture: ComponentFixture<RankPredictorSecondComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RankPredictorSecondComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RankPredictorSecondComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
