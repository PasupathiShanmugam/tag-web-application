import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollegePredictorDetailsComponent } from './college-predictor-details.component';

describe('CollegePredictorDetailsComponent', () => {
  let component: CollegePredictorDetailsComponent;
  let fixture: ComponentFixture<CollegePredictorDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollegePredictorDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollegePredictorDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
